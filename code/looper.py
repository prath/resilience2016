import pandas as pd
import numpy as np
import collections
import matplotlib.pyplot as plt
from scipy import stats
import time
import math
from saxpy import SAX

def compute_sax_rep(data):
    word_size = len(data)
    sax_engine = SAX(wordSize=word_size)
    sax_rep = sax_engine.to_letter_rep(data)[0]
    return sax_rep

def compute_sax_rep_for_all_genes(df):
    all_sax_reps = {}
    for gene in df.columns:
        gene_data = df[gene]
        all_sax_reps[gene] = compute_sax_rep(gene_data.values)
    return all_sax_reps

def compute_sax_distance(d1, d2):
    sax_engine = SAX(wordSize = len(d1))
    return sax_engine.compare_strings(d1, d2)

# Filter training_data to only unique genes
def filter_to_unique_genes(df):
    df.columns = [col.split('.')[0] for col in df.columns] 
    print len(df.columns)
    all_genes = df.columns.unique()
    new_df = pd.DataFrame(columns = all_genes)
    for gene in all_genes:
        chunk = pd.DataFrame(df.ix[:,gene])
        chunk = chunk.median(axis=1)
        new_df.loc[:,gene] = chunk
    return new_df

# Create composite individual by computing median gene expression profiles
def create_composite_profile(df, time_pt_column, columns_to_ignore, scale_profile, time_pt_zero=0):
    unique_time_pts = df[time_pt_column].unique()
    gene_columns = list(set(df.columns) - set(columns_to_ignore) - set(time_pt_column))
    df_composite = pd.DataFrame(columns=df.columns)

    for time_pt in unique_time_pts:
        df_slice = df.query("{}=={}".format(time_pt_column, time_pt))
        df_composite.loc[len(df_composite),:] = df_slice.median()

    if scale_profile==True:
        # Scale composite gene data to Time Point 0
        df_composite_scaled = df_composite[gene_columns] - df_composite.ix[time_pt_zero,gene_columns]
        return df_composite, df_composite_scaled
    else:
        return df_composite

def get_large_ranged_genes(df, min_threshold=1, prctile=10):
    df_range = dict()
    for col in df.columns:
        df_range[col] = max([ abs(df[:1][col].values-df[col].max()), 
                              abs(df[:1][col].values-df[col].min()), 
                              abs(df[-1:][col].values-df[col].max()),
                              abs(df[-1:][col].values-df[col].min())
                            ])[0]
    range_threshold = np.nanpercentile(df_range.values(), 100-prctile)
    if range_threshold < min_threshold:
        range_threshold = min_threshold
    return [col for col in df.columns if df_range[col]>=range_threshold]

def get_phase_shifted_genes(df, PROXIMITY=5, CUTOFF = 0.5, override_offset=False, outside_offset=1):
    euclidean_distance = lambda pt1, pt2: math.sqrt((pt1[0]-pt1[-1])**2 + (pt2[0]-pt2[-1])**2)
    gene_candidates = df.columns
    all_sax_reps = compute_sax_rep_for_all_genes(df)
    phase_shifted_genes = dict()
    if override_offset==True:
        offset = outside_offset
    else:
        offset = int(math.floor(df.shape[0]/4.0))
    
    for gene in gene_candidates:
        print '{} '.format(gene),
        exp_g1 = df[gene]
        sax_rep = all_sax_reps[gene]
        srch_string = sax_rep[0]*offset + sax_rep[:-offset]
        phase_shifted_genes[gene] = []
        # Assume phase-shifting is done by an offset that is equivalent to 90 degrees
        for candidate in gene_candidates:
            if gene!=candidate:
                exp_g2 = df[candidate]
                cand_rep = all_sax_reps[candidate]
                if compute_sax_distance(cand_rep, srch_string) < CUTOFF and \
                    euclidean_distance(exp_g1.values, exp_g2.values)<PROXIMITY:
                    phase_shifted_genes[gene].append(candidate)
        
        if len(phase_shifted_genes[gene])==0:
            del phase_shifted_genes[gene]
    
    return phase_shifted_genes

def get_loop_candidates(df, pctile, prox, over_off=False, out_off=1):
    large_ranged_genes = get_large_ranged_genes(df, prctile=pctile)
    psgenes_dict = get_phase_shifted_genes(df[large_ranged_genes], PROXIMITY=prox, override_offset=over_off, outside_offset=out_off)
    gene_pairs = []
    for gene1 in psgenes_dict.keys():
        for gene2 in psgenes_dict[gene1]:
            gene_pairs.append((gene1,gene2))
    return gene_pairs