# Imports here
import urllib
import urllib2 #allows to open url using urlopen
import gzip #unzips files
import os #removes files
import time #times a function run

import pandas as pd

try:
    import cPickle as pickle
except:
    import pickle

# Version information
__version = 0.21
__version_info = 'Added normalization as separate function'

# Fetch GEO data from NCBI website
def fetch_from_geo(accession_num, mode):
    # Construct query URL
    query_url = "http://www.ncbi.nlm.nih.gov/geo/query/acc.cgi"
    query_params = {}
    query_params['acc'] = 'GSE' + str(accession_num)
    query_params['targ'] = 'all'
    query_params['view'] = 'full'
    query_params['form'] = 'xml'
    url_values = urllib.urlencode(query_params)
    full_url = query_url + '?' + url_values

    # Open connection to URL
    rsrc = urllib2.urlopen(full_url)
    if mode == 'verbose':
        print rsrc.info()

    # Pick suggested filename from GEO
    filename = rsrc.info()['Content-Disposition'].split("=")[1]
    print filename

    # Read data from URL and write to file
    data = rsrc.readlines()
    fp = open(filename, 'wb')
    fp.writelines(data)
    fp.close()

    # Return filename
    return filename

# Fetch GSE Matrix data from GEO
def fetch_gse_matrix_from_geo(accession_num, mode):
    # Construct query URL
    full_url = "ftp://ftp.ncbi.nlm.nih.gov/geo/series/GSE{}nnn/GSE{}/matrix/GSE{}_series_matrix.txt.gz" \
        .format(accession_num / 1000, accession_num, accession_num)

    # Open connection to URL
    rsrc = urllib2.urlopen(full_url)
    if mode == 'verbose':
        print rsrc.info()

    # Pick suggested filename from GEO
    gz_filename = "GSE{}_series_matrix.txt.gz".format(accession_num)

    # Read data from URL and write to file
    data = rsrc.readlines()
    fp = open(gz_filename, 'wb')
    fp.writelines(data)
    fp.close()

    # Gunzip file
    filename = "GSE{}_series_matrix.txt".format(accession_num)
    fp = gzip.open(gz_filename, 'rb')
    fp_out = open(filename, 'wb')
    fp_out.writelines(fp.readlines())
    fp.close()
    fp_out.close()

    # Delete gzipped file
    os.remove(gz_filename)

    # Return filename
    return filename


def fetch_gpl_from_geo(accession_num, mode):
    # ftp://ftp.ncbi.nlm.nih.gov/geo/platforms/GPL4nnn/GPL4133/annot/GPL4133.annot.gz
    # Construct query URL
    if accession_num > 1000:
        full_url = "ftp://ftp.ncbi.nlm.nih.gov/geo/platforms/GPL{}nnn/GPL{}/annot/GPL{}.annot.gz" \
            .format(accession_num / 1000, accession_num, accession_num)
    else:
        full_url = "ftp://ftp.ncbi.nlm.nih.gov/geo/platforms/GPLnnn/GPL{}/annot/GPL{}.annot.gz" \
            .format(accession_num, accession_num)

    # Open connection to URL
    rsrc = urllib2.urlopen(full_url)
    if mode == 'verbose':
        print rsrc.info()

    # Pick suggested filename from GEO
    gz_filename = "GPL{}.annot.gz".format(accession_num)

    # Read data from URL and write to file
    data = rsrc.readlines()
    fp = open(gz_filename, 'wb')
    fp.writelines(data)
    fp.close()

    # Gunzip file  # could we have invoked another function (in common with the gunzip for GEO above) that would unzip
    filename = "GPL{}_annot.txt".format(accession_num)
    fp = gzip.open(gz_filename, 'rb')
    fp_out = open(filename, 'wb')
    fp_out.writelines(fp.readlines())
    fp.close()
    fp_out.close()

    # Delete gzipped file
    os.remove(gz_filename)

    # Return filename
    return filename


# Download all needed GEO datasets
def get_all_data_for_project(meta_project, mode='silent'):
    for gse_num in meta_project:
        print "Accessing Matrix: GSE" + str(gse_num)
        try:
            filename = fetch_gse_matrix_from_geo(gse_num, mode)
            print "Wrote: " + filename
        except urllib2.URLError:
            print "Error: Couldn't access GSE{} matrix data.".format(gse_num)


# Read GEO dataset file into memory
def read_file(filename):
    fp = open(filename, 'rb')
    raw_data = fp.readlines()
    fp.close()
    return raw_data

# Parse GEO dataset into traversable collection
def parse_gse_matrix_file(raw_data):
    # This function will return multiple structures.
    # [1] it will extract the GSE matrix in raw format (i.e. Probe IDs -> Gene Expressions per sample)
    # [2] it will extract metadata for the series
    # [3] it will extract metadata for each sample
    # [4] it will extract platform information (i.e. Probe ID -> Gene ID)
    # It will return a dict object that will hold the parsed structures.

    # Create an empty dict to hold the dataset
    gse_data = {}

    # Extract series metadata
    ii = 0
    series_metadata = {}
    while ii < len(raw_data):
        curr_line = raw_data[ii]

        if "!Series_" in curr_line:
            label, other = curr_line.split('\t', 1)
            # Get metadata label
            discard, mdata_label = label.split('_', 1)
            # Store in series metadata dict
            series_metadata[mdata_label] = other.strip()

        ii += 1
    gse_data['Series_Metadata'] = series_metadata # don't understand this

    # Extract GSE matrix
    mtrx_start, mtrx_stop = [num for num in range(0, len(raw_data)) if '!series_matrix_table' in raw_data[num]]
    gse_raw = map(lambda row: row.strip().split('\t'), raw_data[mtrx_start + 2:mtrx_stop])
    gse_raw_colnames = raw_data[mtrx_start + 1].strip().split('\t')
    gse_raw_colnames = [colname[1:-1] for colname in gse_raw_colnames if '"' in colname]
    gse_matrix = pd.DataFrame.from_records(gse_raw, columns=gse_raw_colnames)
    gse_matrix.iloc[:, 0].str.replace('"', '')
    gse_data['Gene_Exp'] = gse_matrix

    # Extract sample metadata
    smpl_mdata_raw = [row.strip().split('\t') for row in raw_data if '!Sample_' in row]
    for ii in range(0, len(smpl_mdata_raw)):
        smpl_mdata_raw[ii][0] = smpl_mdata_raw[ii][0].split('_', 1)[1]

    metadata = pd.DataFrame(smpl_mdata_raw)
    metadata = metadata.set_index([0])
    metadata.columns = [col_name[1:-1] for col_name in metadata.ix['geo_accession']]
    gse_data['Sample_Metadata'] = metadata

    # Extract GPL Platform ID
    id_idx = [idx for idx in range(0, len(raw_data)) if '!Sample_platform_id' in raw_data[idx]][0]
    gse_data['GPL_ID'] = int(raw_data[id_idx].strip().split('\t')[1].split('GPL', 1)[1][:-1])

    # End of function
    return gse_data


def read_and_parse_gse_matrix_file(filename):
    return parse_gse_matrix_file(read_file(filename))


def parse_gpl_file(raw_data):
    gpl_data = {}

    # Extract GPL data
    mtrx_start, mtrx_stop = [num for num in range(0, len(raw_data)) if '!platform_table_' in raw_data[num]] # this is a list comprehension
    gpl_raw = map(lambda row: row.strip().split('\t'), raw_data[mtrx_start + 2:mtrx_stop])
    gpl_raw_colnames = raw_data[mtrx_start + 1].strip().split('\t')
    gpl_matrix = pd.DataFrame.from_records(gpl_raw, columns=gpl_raw_colnames)
    gpl_data = gpl_matrix

    # End of function
    return gpl_data


def read_and_parse_gpl_file(filename):
    return parse_gpl_file(read_file(filename))


def sync_probe_id_and_gene_symbol(gse_data, gpl_data):
    enhanced_gse_data = gse_data

    # For every row, this should extract GSE's probe ID, store it in a variable, search for the ID in the GPL data, and
    # extract the corresponding Gene_Name and Gene_Title from the GPL data. Once the latter have been extracted, this
    # should append the row with these data items.
    raw_matrix = enhanced_gse_data['Gene_Exp']
    probe_id_list_from_gse = raw_matrix.iloc[:, 0]
    probe_id_list_from_gpl = gpl_data.iloc[:, 0]
    probe_to_symbol = {}

    # Determine the column which has gene symbols
    gene_col_name = \
    [col_name for col_name in ['Gene symbol', 'ILMN_Gene', 'Gene Symbol'] if col_name in gpl_data.columns][0]

    # Hash dict for probe ids from GPL
    for num in range(0, len(probe_id_list_from_gpl)):
        curr_probe_id = probe_id_list_from_gpl[num]
        if len(curr_probe_id) > 1:
            probe_to_symbol[curr_probe_id] = gpl_data[gene_col_name][num]

    gene_symbol_array = []
    for num in range(0, len(probe_id_list_from_gse)):
        curr_probe_id = probe_id_list_from_gse[num]
        if '"' in curr_probe_id:
            curr_probe_id = curr_probe_id[1:-1]
        if len(curr_probe_id) > 1:
            gene_symbol_array.append(probe_to_symbol[curr_probe_id])
        else:
            gene_symbol_array.append('')

    raw_matrix.insert(1, 'Gene_Symbol', gene_symbol_array)

    enhanced_gse_data['Gene_Exp'] = raw_matrix
    enhanced_gse_data['Platform_Data'] = gpl_data

    # End of function: Should return a GSE data structure appended with Gene_Name and Gene_Title
    return enhanced_gse_data


#    # for key in keys:


def find_common_genes_in_project(project, keys):
    #    keys = project.keys()
    all_genes = {}

    for key in keys:
        all_genes[key] = set(project[key]['Gene_Exp']['Gene_Symbol'].unique())

    common_genes = set()

    for key in keys:
        if len(common_genes) == 0:
            common_genes = all_genes[key]
        else:
            common_genes = common_genes.intersection(all_genes[key])

    ncg = set()
    for gene in common_genes:
        if gene != '':
            ncg.add(gene)

    return ncg

def build_project_from_scratch(gse_list):
    meta_project = {}
    ii = 1
    for gse_num in gse_list:
        print "1 - Downloading series matrix file: GSE" + str(gse_num)
        try:
            filename = fetch_gse_matrix_from_geo(gse_num, 'silent')
            print '\t' + "Wrote: " + filename
        except urllib2.URLError:
            print "Error: Couldn't access GSE{} series matrix file.".format(gse_num)

        print "2 - Parsing series matrix file: " + filename # why no try here
        gse_data = read_and_parse_gse_matrix_file(filename)

        print "3 - Downloading platform file: GPL" + str(gse_data['GPL_ID'])
        try:
            filename = fetch_gpl_from_geo(gse_data['GPL_ID'], 'silent')
            print '\t' + "Wrote: " + filename
        except urllib2.URLError:
            print "ERROR: Couldn't access GPL{} platform file.".format(gse_data['GPL_ID'])

        print "4 - Parsing platform file: " + filename
        gpl_data = read_and_parse_gpl_file(filename)

        print "5 - Synchronizing probe id with gene symbols"
        gse_data = sync_probe_id_and_gene_symbol(gse_data, gpl_data)

        print "6 - Storing series information in project object."
        meta_project[str(gse_num)] = gse_data

        print "Done. [{}/{}]".format(ii, len(gse_list))
        ii += 1

    return meta_project


def save_project(meta_project, project_name):
    # Ensure correct name for meta project pickle
    if '.mproj' not in project_name:
        project_name = project_name + '.mproj'

    print "Saving project to file: " + project_name,
    proj_file = open(project_name, 'wb')
    start_time = time.time() # is the time function specific to pickle?
    pickle.dump(meta_project, proj_file) #saving data to the disk
    proj_file.close()
    stop_time = time.time()

    print " - Done. Elapsed time: {0:.2f} secs.".format(stop_time - start_time)

    return project_name


def load_project(project_name):
    # Ensure correct name for meta project pickle
    if '.mproj' not in project_name:
        project_name = project_name + '.mproj'

    print "Loading project from file: " + project_name,
    proj_file = open(project_name, 'rb')
    start_time = time.time()
    meta_project = pickle.load(proj_file)
    proj_file.close()

    stop_time = time.time()

    print " - Done. Elapsed time: {0:.2f} secs.".format(stop_time - start_time)

    return meta_project


def add_to_project(project, data, data_name):
    project[data_name] = data


def build_common_gene_expression_set(project, keys, normalize=True):
    exp_set = pd.DataFrame()
    common_genes = list(find_common_genes_in_project(project, keys))

    for key in keys:
        gse_data = project[key]['Gene_Exp']
        filtered_data = pd.DataFrame()

        ctr = 1
        for gene in common_genes:
            gene_data = gse_data.loc[gse_data['Gene_Symbol'] == gene]
            gene_data = gene_data.iloc[:, 2:].median().transpose()

            if len(filtered_data) > 0:
                filtered_data = pd.concat([filtered_data, gene_data], join='inner', axis=1)
            else:
                filtered_data = gene_data
            if ctr % 1000 == 0:
                print "{}: {}/{}".format(key, ctr, len(common_genes))
            ctr += 1

        filtered_data.columns = common_genes  # Ignoring the empty gene_name

        if len(exp_set) > 0:
            exp_set = exp_set.append(filtered_data)
        else:
            exp_set = filtered_data

    # Perform per-sample normalization


    print "Expression set building complete."
    return exp_set


def get_version():
    return __version


def normalize_expression_set_by_sample(exp_set):
    tps_set = exp_set.transpose()  # Transpose
    tps_norm = (tps_set - tps_set.mean()) / tps_set.std()  # Normalize
    exp_norm = tps_norm.transpose()  # Re-transpose
    return exp_norm
