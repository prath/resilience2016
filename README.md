Looper usage instructions
=========================

Looper is being made available as a Python library, compatible with Python 2.7.x. The Looper library depends on an implementation of SAX in Python by Nathan Hoffman, available at [https://github.com/nphoff/saxpy] under an MIT License.

Datasets need to be preprocessed before being operated on by Looper. Looper takes as input Pandas data frames produced by reading CSV datasets into memory. Specifically, datasets will need to be oriented such that each row represents a single time point, and each column holds gene expression values or values of macro variables such as temperature and weight through time. It is assumed that the input data frames are indexed temporally in ascending order, and that the dataset represents a single individual. Accordingly, the Looper library provides the create\_composite\_profile function to generate a single composite profile out of multiple individuals sampled in the same dataset. Finally, Looper’s loop-finding functions depend on each column in the input dataset having a unique name; the library’s filter\_to\_unique\_genes function will process the dataset and output median-valued columns for every column name that is repeated.
    	The next steps are as follows:
1. Use Pandas to read dataset into memory as DF1. Dataset should fulfill the constraints mentioned above.
2. Filter dataset DF1 into unique genes and store the result as DF2.
3. Typical gene expression datasets have more than 15000 gene columns. Use the get\_large\_ranged\_genes function to obtain as a subset the top N% of all genes when sorted by range, largest first. N is typically 0.5, but can be increased to allow more genes into the subset. This step is not required if the columns have been predetermined in some other way.
4. Filter DF2 into storing the columns selected in Step 3; call this DF3.
5. Use the get\_phase\_shifted\_genes function on DF3 to get gene pairs where one gene is phase-shifted by 90 degrees or T/4 (where T is the total number of time points) with respect to the other. The following control parameters are used:
	- _cutoff_. This function computes the SAX distance of two gene expressions; cutoff imposes a maximum acceptable SAX distance, and is set by default to 0.5. Change this to vary the degree of dissimilarity between genes in gene pairs.
	- _proximity_. This function computes the Euclidean distance between two gene expressions; proximity imposes a maximum acceptable Euclidean distance, and is set by default to 1. Increase this to allow for gene pairs with larger differences in gene expressions over time.
	- _override\_offset_. By default, the function searches within the large-ranged genes subset for genes that are phase-shifted by an offset of T/4. Setting override\_offset to True enables the user to specify an offset of their choice using the next parameter.
	- _outside\_offset_. If override\_offset is set to True, this function uses the offset defined here to search for phase-shifted genes. The user can vary offsets from 1 to T/4 using this parameter to perform a deeper search for phase-shifted genes

